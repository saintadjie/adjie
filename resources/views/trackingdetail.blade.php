@extends('layouts.masterfe')
@push('script-header')
        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('assetsfe/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
      
@endpush
@section('content')
<div class="content-wrapper">
	<div class="row grid-margin">
        <div class="col-lg-12">
            <div class="card">
            	@if (($rs->result) != 1)
        		wew
				@else 
                <div class="card-body">
                	<h4 class="header-title">Informasi detail kurir</h4>
                    <div class="table-responsive">
                        <table id="tb_resi" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">Kurir</th>
                                    <th style="vertical-align: middle; text-align: center;">Nomor Resi</th>
                                    <th style="vertical-align: middle; text-align: center;">Dikirim</th>
                                    <th style="vertical-align: middle; text-align: center;">Diterima</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align: middle; text-align: center;">{{$rsdata->courier}}</td>
                                    <td style="vertical-align: middle; text-align: center;">{{$rsdata->waybill}}</td>
                                    <td style="vertical-align: middle; text-align: center;">{{$rsdata->shipped}}</td>
                                    <td style="vertical-align: middle; text-align: center;">{{$rsdata->received}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                	<h4 class="header-title">Informasi detail tracking</h4>
                    <div class="table-responsive">
                        <table id="tb_tracking" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">Tanggal</th>
                                    <th style="vertical-align: middle; text-align: center;">Deskripsi</th>
                                    <th style="vertical-align: middle; text-align: center;">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($rstracking as $rst)
                                <tr>
                                    <td style="vertical-align: middle; text-align: center;">{{ $rst->date}}</td>
                                    <td style="vertical-align: middle; text-align: center;">{{ $rst->desc}}</td>
                                    <td style="vertical-align: middle; text-align: center;">{{$rst->status}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
          		@endif
	        </div>
	    </div>
	</div>
</div>

@push('script-footer')
        <script src="{{url('assetsfe/vendors/datatables.net/jquery.dataTables.js')}}"></script>
		<script src="{{url('assetsfe/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
		<script src="{{url('assetsfe/js/data-table.js')}}"></script>

        <script type="text/javascript">

            $('#tb_tracking').DataTable({
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
                "order": [[ 0, "desc" ]],
            });

        </script>
@endpush
@endsection