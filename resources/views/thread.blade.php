@extends('layouts.masterfe')
@push('script-header')

@endpush
@section('content')
<div class="content-wrapper">
	<div class="row grid-margin">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  	<h4 class="card-title">50 Hot Threads Kaskus</h4>
                  	<p class="card-text">
                    	Kumpulan hot threads Kaskus yang datanya diambil secara live dari Kaskus
                  	</p>
				    </nav>
					<div class="card-columns">
						@foreach($result as $results)
						<div class="card">
							<img class="card-img-top" src="{{$results->image}}" alt="adjie.id">
							<div class="card-body">
								<h4 class="card-title mt-0 text-justify">
									@if (!empty($results->title))
			                		{{$results->title}}
									@else 
									Tidak Ada Judul
					          		@endif
								</h4>
								<hr/>
				                <nav>
				                	<ul class="d-flex flex-wrap justify-content-center">
				                		<div class="badge badge-danger badge-pill">KASKUS</div>
				                		&nbsp;&nbsp;
				                		<div class="badge badge-success badge-pill">
					                		@if (!empty($results->forum_name))
					                		{{$results->forum_name}}
											@else 
											Tidak Diketahui
							          		@endif
						          		</div>
						        	</ul>
							        <ul class="d-flex flex-wrap justify-content-center">
							          	<a href="{{url('/threaddetail/'.$results->slug_title)}}" target="_blank" class="btn btn-primary">Selengkapnya...</a>
							        </ul>
						    	</nav>
								
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav>
        <ul class="pagination d-flex flex-wrap justify-content-center pagination-danger">
          	{{ $result->links() }}
        </ul>
    </nav>
</div>
<!-- content-wrapper ends -->

@push('script-footer')
<script src="{{url('assetsfe/jsuser/thread.js')}}"></script>
@endpush
@endsection