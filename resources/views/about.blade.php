@extends('layouts.masterfe')
@section('content')
	<div class="content-wrapper">
		<div class="row">
			<div class="col-md-8 grid-margin grid-margin-md-0 stretch-card mx-auto">
				<div class="card">
					<div class="card-body text-center">
						<div>
							@if($rs == NULL)
                                <img class="mx-auto img-fluid img-thumbnail rounded-circle" src="{{url('images\noimage.jpg')}}" alt="User" style="height: 240px; width: 240px">
                            @else
                                @if($rs->photo != NULL)
                                    <img class="mx-auto img-fluid img-thumbnail rounded-circle" src="{{url($rs->photo)}}" alt="User" style="height: 240px; width: 240px">
                                @else
                                    <img class="mx-auto img-fluid img-thumbnail rounded-circle" src="{{url('images\noimage.jpg')}}" alt="User" style="height: 240px; width: 240px">
                                @endif
                            @endif
                            <hr/>
							@if($rs == NULL)
								<h4>-</h4>
							@else
								<h4>{{$rs->name}}</h4>
							@endif
							<p class="text-muted mb-0">
								@if($rs == NULL)
									-
								@else
									{{$rs->website}}
								@endif
							</p>
						</div>
						<p class="mt-2 card-text">
							@if($rs == NULL)
								-
							@else
								{{$rs->deskripsi}}
							@endif
						</p>
						<div class="template-demo">
							<button id="btn_facebook" type="button" class="btn btn-social-icon btn-facebook btn-rounded" value="wew"><i class="mdi mdi-facebook"></i></button>
		                    <button id="btn_instagram" type="button" class="btn btn-social-icon btn-primary btn-rounded"><i class="mdi mdi-instagram"></i></button>                                        
		                    <button id="btn_gmail" type="button" class="btn btn-social-icon btn-google btn-rounded"><i class="mdi mdi-gmail"></i></button>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@push('script-footer')
        <script type="text/javascript">
        	var facebook 	= '{{isset($rs->facebook) ? $rs->facebook : null}}';
        	var instagram 	= '{{isset($rs->instagram) ? $rs->instagram : null}}';
        	var gmail 		= '{{isset($rs->email) ? $rs->email : null}}';

        	$('#btn_facebook').click(function() {
        		window.open('https://facebook.com/'+ facebook, '_blank');
			});
			$('#btn_instagram').click(function() {
			    window.open('https://instagram.com/'+ instagram, '_blank');
			});
        	$('#btn_gmail').click(function() {
			    window.open('mailto:'+ gmail, '_blank');
			});
        </script>
@endpush
@endsection