@extends('layouts.masterfe')
@push('script-header')
<link rel="stylesheet" href="{{url('assetsfe/vendors/sweetalert2/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-8 grid-margin stretch-card mx-auto">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Kontak saya</h4>
                    <p class="card-description">
                        Isi semua data untuk megirim pesan kepada saya
                    </p><!-- 
                    @if(\Session::has('alert-failed'))
                        <div class="alert alert-failed">
                            <div>{{Session::get('alert-failed')}}</div>
                        </div>
                    @endif
                    @if(\Session::has('alert-success'))
                        <div class="alert alert-success">
                            <div>{{Session::get('alert-success')}}</div>
                        </div>
                    @endif -->
                    <form class="forms-sample" id="form_contact">
                        <div class="form-group">
                            <label for="email">Email anda</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Nama anda</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama" required>
                        </div>
                        <div class="form-group">
                            <label for="judul">Judul Pesan</label>
                            <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Pesan" required>
                        </div>
                        <div class="form-group">
                            <label for="pesan">Isi Pesan</label>
                            <textarea class="form-control" id="pesan" name="pesan" rows="4" required></textarea>
                        </div>
                        <div class="row float-right">
                            <button type="button" class="btn btn-primary mr-2 pull-right" id="btn_simpan">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script-footer')
<script src="{{url('assetsfe/vendors/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{url('assetsfe/js/loadingoverlay.min.js')}}"></script>
<script src="{{url('assetsfe/jsuser/contact.js')}}"></script>
<script type="text/javascript">

    var url_api         = "{{url('/sendemail')}}"
    var url_contact     = "{{url('/contact')}}"
</script>
@endpush
@endsection