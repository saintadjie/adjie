
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="Inspirasi seputar Anime, Manga, Software, Games dan berbagai macam Tutorial Website, Blogger, SEO, Responsive, Gallery, jQuery, CSS, HTML, JavaScript, Widget, Web Tools disertai Tips dan Trik.">
        <meta name="description" content="Personal Blog Adjie">
        <title>adjie.id - Administrator</title>
        <meta content="adjie.id" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{url('assetsbe/vendors/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{url('assetsbe/vendors/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href="{{url('assetsbe/css/vertical-layout-light/style.css')}}">
        <link rel="stylesheet" href="{{url('assetsbe/vendors/sweetalert2/sweetalert2.min.css')}}">
        <link rel="shortcut icon" href="{{url('assetsbe/images/logo.png')}}" />
        @stack('script-header')
    </head>

    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="navbar-brand-wrapper align-items-center">
                    <a class="navbar-brand brand-logo" href="{{url('administrator/home')}}"><img src="{{url('assetsfe/images/logodark.png')}}" alt="logo"/></a>
                    <a class="navbar-brand brand-logo-mini" href="{{url('administrator/home')}}"><img src="{{url('assetsfe/images/logo.png')}}" alt="logo"/></a>
                    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                        <span class="mdi mdi-sort-variant"></span> 
                    </button>
                </div>


      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item count-indicator nav-profile dropdown">
            <span class="count">3</span>
            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <span class="nav-profile-name">Hi, {{ Auth::user()->name }}</span>
              <img src="{{url('assetsbe/images/logo.png')}}" alt="profile"/>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item text-primary">
                <i class="mdi mdi-account"></i>
                Profile
              </a>
              <a href="{{ route('logout') }}" class="dropdown-item text-primary">
                <i class="mdi mdi-logout text-primary"></i>
                Keluar
              </a>
            </div>
          </li>
          
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <div class="theme-setting-wrapper">
        <div id="settings-trigger"><i class="mdi mdi-settings"></i></div>
        <div id="theme-settings" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <p class="settings-heading">SIDEBAR SKINS</p>
          <div class="sidebar-bg-options" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
          <div class="sidebar-bg-options selected" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
          <p class="settings-heading mt-2">HEADER SKINS</p>
          <div class="color-tiles mx-0 px-4">
            <div class="tiles success"></div>
            <div class="tiles warning"></div>
            <div class="tiles danger"></div>
            <div class="tiles primary"></div>
            <div class="tiles info"></div>
            <div class="tiles dark"></div>
            <div class="tiles default"></div>
          </div>
        </div>
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="dropdown sidebar-profile-dropdown">
          <a class="dropdown-toggle d-flex align-items-center justify-content-between" href="#" data-toggle="dropdown" id="profileDropdown1">
            <img src="{{url('assetsbe/images/logo.png')}}" alt="profile" class="sidebar-profile-icon"/>
            <div>
                <div class="nav-profile-name">{{ Auth::user()->name }}</div>
            </div>
          </a>
          <div class="dropdown-menu navbar-dropdown dropdown-menu-left" aria-labelledby="profileDropdown1">
            <a class="dropdown-item">
              <i class="mdi mdi-account"></i>
              Profil
            </a>
            <a href="{{ route('logout') }}" class="dropdown-item">
              <i class="mdi mdi-logout"></i>
              Keluar
            </a>
          </div>
        </div>
        <ul class="nav">
          <li class="nav-item">
            <div class="sidebar-title">Main</div>
            <a class="nav-link" href="{{url('administrator/home')}}">
              <i class="mdi mdi-cards-variant menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/')}}" target="_blank">
              <i class="mdi mdi-eye menu-icon"></i>
              <span class="menu-title">Lihat Website</span>
            </a>
          </li>

          <li class="nav-item">
            <div class="sidebar-title">Menu</div>
            <a class="nav-link" href="{{url('administrator/about')}}">
              <i class="mdi mdi-face menu-icon"></i>
              <span class="menu-title">About</span>
            </a>
          </li>
          <li class="nav-item">
            <div class="sidebar-title">Menu</div>
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-heart-outline menu-icon"></i>
              <span class="menu-title">Components</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/accordions.html">Accordions</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/badges.html">Badges</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/breadcrumbs.html">Breadcrumbs</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Dropdowns</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/modals.html">Modals</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/progress.html">Progress bar</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/pagination.html">Pagination</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/tabs.html">Tabs</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/typography.html">Typography</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/tooltips.html">Tooltips</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/popups.html">Popups</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/notifications.html">Notifications</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-advanced" aria-expanded="false" aria-controls="ui-advanced">
              <i class="mdi mdi-folder-outline menu-icon"></i>
              <span class="menu-title">Plugins</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-advanced">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dragula.html">Dragula</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/clipboard.html">Clipboard</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/context-menu.html">Context menu</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/slider.html">Sliders</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/carousel.html">Carousel</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/colcade.html">Colcade</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/loaders.html">Loaders</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/widgets/widgets.html">Widgets</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
              <i class="mdi mdi-view-grid menu-icon"></i>
              <span class="menu-title">Forms</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a></li>                
                <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Advanced Elements</a></li>
                <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Validation</a></li>
                <li class="nav-item"><a class="nav-link" href="pages/forms/wizard.html">Wizard</a></li>
                <li class="nav-item"><a class="nav-link" href="pages/forms/text_editor.html">Text editors</a></li>
                <li class="nav-item"><a class="nav-link" href="pages/forms/code_editor.html">Code editors</a></li>
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <div class="sidebar-title">Web application</div>
            <a class="nav-link" href="pages/apps/calendar.html">
              <i class="mdi mdi-calendar-blank menu-icon"></i>
              <span class="menu-title">Calendar</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}">
              <i class="mdi mdi-logout-variant menu-icon"></i>
              <span class="menu-title">Keluar</span>
            </a>
          </li>
          
          
          
        </ul>
        <div class="designer-info">
            Designed by: <a href="https://adjie.id/" target="_blank">adjie.id</a>
        </div>
      </nav>
      <!-- partial -->
      <div class="main-panel">
          
          @yield('content')
          
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="justify-content-center justify-content-sm-between">
            <span class="text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020. All rights reserved.</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

        <!-- base:js -->
        <script src="{{url('assetsbe/vendors/js/vendor.bundle.base.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page-->
        <script src="{{url('assetsbe/vendors/chart.js/Chart.min.js')}}"></script>
        <!-- End plugin js for this page-->
        <!-- inject:js -->
        <script src="{{url('assetsbe/js/off-canvas.js')}}"></script>
        <script src="{{url('assetsbe/js/hoverable-collapse.js')}}"></script>
        <script src="{{url('assetsbe/js/template.js')}}"></script>
        <script src="{{url('assetsbe/js/settings.js')}}"></script>
        <script src="{{url('assetsbe/js/todolist.js')}}"></script>
        <!-- endinject -->
        <!-- plugin js for this page -->
        <!-- End plugin js for this page -->
        <!-- Custom js for this page-->
        <script src="{{url('assetsbe/js/dashboard.js')}}"></script>
        <script src="{{url('assetsbe/vendors/sweetalert2/sweetalert2.min.js')}}"></script>

        @stack('script-footer')
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-Requested-With': 'XMLHttpRequest',
                }
            })
        </script>
        <!-- End custom js for this page-->
    </body>
</html>

