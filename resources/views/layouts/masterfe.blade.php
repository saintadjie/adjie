<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="Inspirasi seputar Anime, Manga, Software, Games dan berbagai macam Tutorial Website, Blogger, SEO, Responsive, Gallery, jQuery, CSS, HTML, JavaScript, Widget, Web Tools disertai Tips dan Trik.">
        <meta name="description" content="Personal Blog Adjie">
        <title>adjie.id</title>
        <meta content="adjie.id" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{url('assetsfe/vendors/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{url('assetsfe/vendors/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href="{{url('assetsfe/css/horizontal-layout-light/style.css')}}">
        <link rel="shortcut icon" href="{{url('assetsfe/images/logo.png')}}" />
        @stack('script-header')
    </head>

    <body>
        <div class="container-scroller">
            <!-- partial:partials/_horizontal-navbar.html -->
            <div class="horizontal-menu">
                <nav class="navbar top-navbar col-lg-12 col-12 p-0">
                    <div class="container">

                        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                            <a class="navbar-brand brand-logo" href="{{url('/')}}"><img src="{{url('assetsfe/images/logolight.png')}}" alt="logo"/></a>
                            <a class="navbar-brand brand-logo-mini" href="{{url('/')}}"><img src="{{url('assetsfe/images/logo.png')}}" alt="logo"/></a>
                        </div>

                        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                            

                            <ul class="navbar-nav navbar-nav-right">
                                <li class="nav-item">
                                    <a href="{{url('/about')}}" class="nav-link">
                                        <i class="mdi mdi-account-star menu-icon"></i>
                                        <span class="menu-title">About</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('/contact')}}" class="nav-link">
                                        <i class="mdi mdi-contact-mail menu-icon"></i>
                                        <span class="menu-title">Contact</span>
                                    </a>
                                </li>
                            </ul>

                            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                                <span class="mdi mdi-menu"></span>
                            </button>
                        </div>
                    </div>
                </nav>

                <nav class="bottom-navbar">
                    <div class="container">
                        <ul class="nav page-navigation">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/')}}">
                                    <i class="mdi mdi-view-dashboard-outline menu-icon"></i>
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/corona')}}">
                                    <i class="mdi mdi-brightness-7 menu-icon"></i>
                                    <span class="menu-title">Info Corona</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/tracking')}}">
                                    <i class="mdi mdi-radar menu-icon"></i>
                                    <span class="menu-title">Tracking/Check Resi</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/thread')}}">
                                    <i class="mdi mdi mdi-cash-multiple menu-icon"></i>
                                    <span class="menu-title">Threads</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                                    <span class="menu-title">Forms</span>
                                    <i class="menu-arrow"></i>
                                </a>
                                <div class="submenu">
                                    <ul class="submenu-item">
                                        <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Advanced Elements</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Validation</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/forms/wizard.html">Wizard</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/forms/text_editor.html">Text Editor</a></li>
                                        <li class="nav-item"><a class="nav-link" href="pages/forms/code_editor.html">Code Editor</a></li>
                                    </ul>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="{{url('/disclaimer')}}" class="nav-link">
                                    <i class="mdi mdi-clipboard-alert menu-icon"></i>
                                    <span class="menu-title">Disclaimer</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="{{url('/privacypolicy')}}" class="nav-link">
                                    <i class="mdi mdi-file-lock menu-icon"></i>
                                    <span class="menu-title">Privacy Policy</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <div class="main-panel">
                    
                  
                        @yield('content')

                    
                    <!-- content-wrapper ends -->
                    <!-- partial:partials/_footer.html -->
                    <footer class="footer">
                        <div class="container">
                            <div class="w-100 clearfix">
                                <span class="d-block text-center text-sm-left d-sm-inline-block">Copyright © 2020 <a href="https://adjie.id/">adjie.id</a>. All rights reserved.</span>
                                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart-outline text-danger"></i></span>
                            </div>
                        </div>
                    </footer>
                    <!-- partial -->
                </div>
                <!-- main-panel ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->

        <!-- base:js -->
        <script src="{{url('assetsfe/vendors/js/vendor.bundle.base.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page-->
        <script src="{{url('assetsfe/vendors/chart.js/Chart.min.js')}}"></script>
        <!-- End plugin js for this page-->
        <!-- inject:js -->
        <script src="{{url('assetsfe/js/off-canvas.js')}}"></script>
        <script src="{{url('assetsfe/js/hoverable-collapse.js')}}"></script>
        <script src="{{url('assetsfe/js/template.js')}}"></script>
        <script src="{{url('assetsfe/js/settings.js')}}"></script>
        <script src="{{url('assetsfe/js/todolist.js')}}"></script>
        <!-- endinject -->
        <!-- plugin js for this page -->
        <!-- End plugin js for this page -->
        <!-- Custom js for this page-->
        <script src="{{url('assetsfe/js/dashboard.js')}}"></script>
        @stack('script-footer')
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-Requested-With': 'XMLHttpRequest',
                }
            })
        </script>
        <!-- End custom js for this page-->
    </body>
</html>