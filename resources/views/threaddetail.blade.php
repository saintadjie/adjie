@extends('layouts.masterfe')
@push('script-header')

@endpush
@section('content')
<div class="content-wrapper">
    <div class="row">
    	<div class="col-md-12 grid-margin stretch-card mx-auto">
    		<div class="card">
    			<div class="card-body">
					<nav aria-label="breadcrumb">
				      	<ol class="breadcrumb bg-dark">
					        <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
					        <li class="breadcrumb-item"><a href="{{url('/thread')}}">Threads</a></li>
					        <li class="breadcrumb-item active" aria-current="page">
					        	@if (!empty($result->title))
		                		{{ $result->title }}
								@else 
								Tidak Ada Judul
				          		@endif
					        </li>
				      	</ol>
				    </nav>
				    <div class="row">
				    	<div class="col-md-4 grid-margin">
							<div class="card bg-facebook d-flex align-items-center">
								<div class="card-body">
									<div class="d-flex flex-row align-items-center">
										<div class="ml-3">
											<h6 class="text-white">Sumber :</h6>
											<p class="mt-2 text-white card-text">KASKUS</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 grid-margin">
							<div class="card bg-linkedin d-flex align-items-center">
								<div class="card-body">
									<div class="d-flex flex-row align-items-center">
										<div class="ml-3">
											<h6 class="text-white">Forum :</h6>
											<p class="mt-2 text-white card-text">
											@if (!empty($result->forum_name))
					                		{{ $result->forum_name }}
											@else 
											Tidak Diketahui
							          		@endif
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 grid-margin">
							<div class="card bg-twitter d-flex align-items-center">
								<div class="card-body">
									<div class="d-flex flex-row align-items-center">
										<img src="{{ $result->profile_picture }}" class="img-xs rounded" alt="adjie.id"/>
										<div class="ml-3">
											<h6 class="text-white">Oleh :</h6>
											<p class="mt-2 text-white card-text">
											@if (!empty($result->username))
					                		{{ $result->username }}
											@else 
											Tidak Diketahui
							          		@endif
							          		</p>
										</div>
									</div>
								</div>
							</div>
						</div>
				    </div>
				    <div class="row">
						<div class="col-md-12 grid-margin stretch-card">
							<div class="card">
								<div class="card-body">
									<div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
										<img class="image-tile col-xl-4 col-lg-4 col-md-4 col-md-12 col-12" src="{{$result->image}}" alt="adjie.id">
										<div class="col-xl-8 col-lg-8 col-md-8 col-md-12 col-12">
											<h6 class="mb-0">
												@if (!empty($result->title))
						                		{{ $result->title }}
												@else 
												Tidak Ada Judul
								          		@endif
											</h6>
											<br/>
											<p class="mb-0 text-muted">
												@if (!empty($result->description))
						                		{{ $result->description }}
												@else 
												Tidak Ada Deskripsi
								          		@endif
								          	</p>
											<br/>
											<a href="{{ $result->url }}" target="_blank" class="btn btn-primary">Selengkapnya...</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
    		</div>
	        
		</div>
	</div>
    
	
	{{ $result->slug_title }}
	
</div>
<!-- content-wrapper ends -->

@push('script-footer')
@endpush
@endsection