@extends('layouts.masterfe')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-8 grid-margin stretch-card mx-auto">
            <div class="card">
                <div class="card-body">
                  	<h4 class="card-title">Disclaimer</h4>
                  	<p class="card-description">Disclaimer <code>https://adjie.id</code></p>
                  	<div class="mt-4">
                    	<div class="accordion accordion-multi-colored" id="disclaimer" role="tablist">
                      		<div class="card">
                        		<div class="card-header" role="tab" id="heading1">
                          			<h6 class="mb-0">
                            			<a class="collapse" data-toggle="collapse" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                              				Disclaimer
                            			</a>
                          			</h6>
                        		</div>
                        		<div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#disclaimer">
                          			<div class="card-body">
                            			<p class="mb-0">This disclaimer ("Disclaimer", "Agreement") is an agreement between Website Operator ("Website Operator", "us", "we" or "our") and you ("User", "you" or "your"). This Disclaimer sets forth the general guidelines, terms and conditions of your use of the adjie.id website and any of its products or services (collectively, "Website" or "Services").</p>
			                        </div>
                        		</div>
                      		</div>
                      		<div class="card">
                        		<div class="card-header" role="tab" id="heading2">
                          			<h6 class="mb-0">
                            			<a data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                              				Representation
                            			</a>
                          			</h6>
                        		</div>
                        		<div id="collapse2" class="collapse" role="tabpanel" aria-labelledby="heading2" data-parent="#disclaimer">
                          			<div class="card-body">
                            			<p class="mb-0">Any views or opinions represented in this Website belong solely to the Content creators and do not represent those of people, institutions or organizations that the Website Operator or creators may or may not be associated with in professional or personal capacity, unless explicitly stated. Any views or opinions are not intended to malign any religion, ethnic group, club, organization, company, or individual.</p>
			                        </div>
                        		</div>
                      		</div>
                      		<div class="card">
                        		<div class="card-header" role="tab" id="heading3">
                          			<h6 class="mb-0">
                            			<a data-toggle="collapse" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                              				Content and postings
                            			</a>
                          			</h6>
                        		</div>
                        		<div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="heading3" data-parent="#disclaimer">
                          			<div class="card-body">
                            			<p class="mb-0">You may not modify, print or copy any part of the Website. Inclusion of any part of this Website in another work, whether in printed or electronic or another form or inclusion of any part of the Website in another website by embedding, framing or otherwise without the express permission of Website Operator is prohibited.</p>
                                        <br/>
                                        <p class="mb-0">You may submit comments for the Content available on the Website. By uploading or otherwise making available any information to Website Operator, you grant Website Operator the unlimited, perpetual right to distribute, display, publish, reproduce, reuse and copy the information contained therein. You may not impersonate any other person through the Website. You may not post content that is defamatory, fraudulent, obscene, threatening, invasive of another person's privacy rights or that is otherwise unlawful. You may not post content that infringes on the intellectual property rights of any other person or entity. You may not post any content that includes any computer virus or other code designed to disrupt, damage, or limit the functioning of any computer software or hardware.</p>
			                        </div>
                        		</div>
                      		</div>
                      		<div class="card">
                        		<div class="card-header" role="tab" id="heading4">
                          			<h6 class="mb-0">
                            			<a data-toggle="collapse" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                              				Indemnification and warranties
                            			</a>
                          			</h6>
                        		</div>
                        		<div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="heading4" data-parent="#disclaimer">
                          			<div class="card-body">
                            			<p class="mb-0">While we have made every attempt to ensure that the information contained on the Website is correct, Website Operator is not responsible for any errors or omissions, or for the results obtained from the use of this information. All information on the Website is provided "as is", with no guarantee of completeness, accuracy, timeliness or of the results obtained from the use of this information, and without warranty of any kind, express or implied. In no event will Website Operator, or its partners, employees or agents, be liable to you or anyone else for any decision made or action taken in reliance on the information on the Website or for any consequential, special or similar damages, even if advised of the possibility of such damages. Information on the Website is for general information purposes only and is not intended to provide legal, financial, medical, or any other type of professional advice. Please seek professional assistance should you require it. Furthermore, information contained on the Website and any pages linked to and from it are subject to change at any time and without warning.</p>
                                        <br/>
                                        <p class="mb-0">We reserve the right to modify this Disclaimer relating to the Website or Services at any time, effective upon posting of an updated version of this Disclaimer on the Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Website after any such changes shall constitute your consent to such changes. Policy was created with WebsitePolicies.</p>
			                        </div>
                        		</div>
                      		</div>
                      		<div class="card">
                        		<div class="card-header" role="tab" id="heading5">
                          			<h6 class="mb-0">
                            			<a data-toggle="collapse" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                              				Acceptance of this disclaimer
                            			</a>
                          			</h6>
                        		</div>
                        		<div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#disclaimer">
                          			<div class="card-body">
                            			<p class="mb-0">You acknowledge that you have read this Disclaimer and agree to all its terms and conditions. By accessing the Website you agree to be bound by this Disclaimer. If you do not agree to abide by the terms of this Disclaimer, you are not authorized to use or access the Website.</p>
			                        </div>
                        		</div>
                      		</div>
                      		<div class="card">
                        		<div class="card-header" role="tab" id="heading6">
                          			<h6 class="mb-0">
                            			<a data-toggle="collapse" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                              				Contacting us
                            			</a>
                          			</h6>
                        		</div>
                        		<div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#disclaimer">
                          			<div class="card-body">
                            			<p class="mb-0">If you would like to contact us to understand more about this Disclaimer or wish to contact us concerning any matter relating to it, you may do so via the contact form</p>
			                        </div>
                        		</div>
                      		</div>
                      		
                      		<p class="text-danger">
                              	<i class="mdi mdi-alert-octagon mr-2"></i>This document was last updated on April 10, 2020
                            </p>
                    	</div>
                  	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->

@endsection