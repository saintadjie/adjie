@extends('layouts.masterbe')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Halaman About</h4>
                        <form class="form-sample" id="form_about">
                            <p class="card-description">
                                Diisi untuk identitas website di halaman about
                            </p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4 grid-margin stretch-card mx-auto">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
                                                    @if($rs == NULL)
                                                        <img class="img-tile mx-auto img-fluid" src="{{url('images\noimage.jpg')}}" alt="User" height="180">
                                                    @else
                                                        @if($rs->photo != NULL)
                                                            <img class="img-tile mx-auto img-fluid" src="{{url($rs->photo)}}" alt="User" height="180">
                                                        @else
                                                            <img class="img-tile mx-auto img-fluid" src="{{url('images\noimage.jpg')}}" alt="User" height="180">
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Nama</label>
                                        @if($rs == NULL) 
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Nama">
                                            </div>
                                        @else
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="{{ $rs->name }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Instagram</label>
                                        @if($rs == NULL) 
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram">
                                            </div>
                                        @else
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram" value="{{ $rs->instagram }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Website</label>
                                        @if($rs == NULL) 
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="website" name="website" placeholder="Website">
                                            </div>
                                        @else
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="website" name="website" placeholder="Website" value="{{ $rs->website }}">
                                            </div>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Facebook</label>
                                        @if($rs == NULL) 
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook">
                                            </div>
                                        @else
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook" value="{{ $rs->facebook }}">
                                            </div>
                                        @endif
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Email</label>
                                        @if($rs == NULL) 
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                            </div>
                                        @else
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ $rs->email }}">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Photo</label>
                                        <input type="file" accept=".jpg,.jpeg,.png" id="photo" name="photo" class="file-upload-default">
                                        <div class="input-group col-sm-9">
                                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Photo">
                                            <span class="input-group-append">
                                                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Deskripsi</label>
                                        @if($rs == NULL) 
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="8"></textarea>
                                            </div>
                                        @else
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="8">{{ $rs->deskripsi }}</textarea>
                                            </div>
                                        @endif
                                    </div>
                                 </div>
                            </div>
                            <div class="row" hidden>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Penulis</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="penulis" name="penulis" placeholder="Penulis" value="{{ Auth::user()->name }}" readonly>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                            <div class="row float-right">
                                <button type="button" class="btn btn-lg btn-primary mr-2 pull-right" id="btn_simpan">Simpan</button>
                            </div>
                    
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@push('script-footer')
<script src="{{url('assetsbe/js/file-upload.js')}}"></script>
<script src="{{url('assetsbe/jsadmin/about.js')}}"></script>
<script type="text/javascript">

    var url_api     = "{{url('api/v1/administrator/saveabout')}}"
    var url_about   = "{{url('/administrator/about')}}"
</script>
@endpush
@endsection
