@extends('layouts.masterbe')

@section('content')
    <div class="welcome-message">
        <div class="d-lg-flex justify-content-between align-items-center">
            <div class="pr-5 image-border"><img src="{{url('assetsbe/images/logobanner.png')}}" alt="welcome"></div>
            <div class="pl-8">
                <h2 class="text-white font-weight-bold mb-3">Selamat datang {{Auth::User()->name}}</h2>
                <p class="pb-0 mb-1">Disini anda bisa melakukan segala yang anda mau sesuai menu yang ada.</p>
                <p>Selamat bersenang-senang.</p>
            </div>
            <div class="pl-4">
                <button type="button" class="btn btn-primary" id="skip-mesages">Skip</button>
            </div>
        </div>
    </div>
        
    <hr/>
    <div class="row grid-margin">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Galeri</h4>
                    <div class="owl-carousel owl-theme lazy-load">
                        <img class="owl-lazy" src="http://www.urbanui.com/wagondash/template/images/carousel/banner_8.jpg" data-src="http://www.urbanui.com/wagondash/template/images/carousel/banner_8.jpg" data-src-retina="http://www.urbanui.com/wagondash/template/images/carousel/banner_8.jpg" alt="image"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
