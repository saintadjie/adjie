<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Email adjie.id</title>
    <style>
        table.greenTable {
          font-family: Georgia, serif;
          border: 2px solid #0088ff;
          background-color: #D4EED1;
          width: 100%;
          text-align: center;
        }
        table.greenTable td, table.greenTable th {
          border: 1px solid #24943A;
          padding: 3px 2px;
        }
        table.greenTable tbody td {
          font-size: 12px;
        }
        table.greenTable thead {
          background: #24943A;
          background: -moz-linear-gradient(top, #5baf6b 0%, #3a9e4d 66%, #24943A 100%);
          background: -webkit-linear-gradient(top, #5baf6b 0%, #3a9e4d 66%, #24943A 100%);
          background: linear-gradient(to bottom, #5baf6b 0%, #3a9e4d 66%, #24943A 100%);
          border-bottom: 0px solid #444444;
        }
        table.greenTable thead th {
          font-size: 14px;
          font-weight: bold;
          color: #F0F0F0;
          text-align: left;
          border-left: 2px solid #24943A;
        }
        table.greenTable thead th:first-child {
          border-left: none;
        }

        table.greenTable tfoot {
          font-size: 11px;
          font-weight: bold;
          color: #F0F0F0;
          background: #24943A;
          background: -moz-linear-gradient(top, #5baf6b 0%, #3a9e4d 66%, #24943A 100%);
          background: -webkit-linear-gradient(top, #5baf6b 0%, #3a9e4d 66%, #24943A 100%);
          background: linear-gradient(to bottom, #5baf6b 0%, #3a9e4d 66%, #24943A 100%);
          border-top: 1px solid #24943A;
        }
        table.greenTable tfoot td {
          font-size: 11px;
        }
        table.greenTable tfoot .links {
          text-align: right;
        }
        table.greenTable tfoot .links a{
          display: inline-block;
          background: #FFFFFF;
          color: #24943A;
          padding: 2px 8px;
          border-radius: 5px;
        }
    </style>
</head>
<body>
    Pesan dari adjie.id
    <br/>
    <table class="greenTable">
        <thead>
            <tr>
                <th>Dikirim oleh : {{ $name }}</th>
            </tr>
        </thead>
        <tbody>
            <tr style="text-align: center;">
                <td>isi pesan</td>
            </tr>
            <tr>
                <td>{{ $pesan }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <div class="links">Pesan telah disampaikan ke <a href="https://adjie.id">adjie.id</a></div>
                </td>
            </tr>
        </tfoot>
    </table>
</body>
</html>