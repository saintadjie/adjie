@extends('layouts.masterfe')
@push('script-header')
<link rel="stylesheet" href="{{url('assetsfe/vendors/sweetalert2/sweetalert2.min.css')}}">
<link rel="stylesheet" href="{{url('assetsfe/vendors/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{url('assetsfe/vendors/select2-bootstrap-theme/select2-bootstrap.min.css')}}">
@endpush
@section('content')
<div class="content-wrapper">
	<div class="col-8 grid-margin stretch-card mx-auto">
	    <div class="card">
	        <div class="card-body">
	          	<h4 class="card-title">Cek Resi</h4>
	          	<p class="card-description">
	            	Masukkan nomor resi dan kurir anda untuk tracking barang anda
	          	</p>
	          	<form action="{{ route('trackingdetail') }}" method="GET" class="forms-sample" id="form_cekresi">
	            	<div class="form-group">
	              		<label for="nomorresi">Nomor Resi</label>
	              		<input type="text" class="form-control" id="nomorresi" name="nomorresi" placeholder="Nomor Resi" required>
	            	</div>
	            	<div class="form-group">
	              		<label for="kurir">Kurir</label>
	            		<select class="form-control select2" id="kurir" name="kurir" style="width: 100%" required>
	            			<option value="" selected disabled>--- Pilih Kurir ---</option>
	              			<option value="anteraja">ANTERAJA</option>
	              			<option value="jne">JNE</option>
	              			<option value="jnt">JNT</option>
	              			<option value="lex">LEX (LAZADA EXPRESS)</option>
	              			<option value="lion">LION PARCEL</option>
	              			<option value="ninja">NINJA XPRESS</option>
	              			<option value="pos">POS INDONESIA</option>
	              			<option value="sicepat">SICEPAT</option>
	              			<option value="tiki">TIKI</option>
	              			<option value="wahana">WAHANA</option>
	            		</select>
	          		</div>
	            	<div class="row float-right">
                        <button type="submit" class="btn btn-lg btn-primary mr-2 pull-right" id="btn_cari">Cek Resi</button>
                    </div>
	        	</form>
	    	</div>
		</div>
	</div>
</div>
<!-- content-wrapper ends -->

@push('script-footer')
<script src="{{url('assetsfe/vendors/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{url('assetsfe/js/loadingoverlay.min.js')}}"></script>
<script src="{{url('assetsfe/vendors/select2/select2.min.js')}}"></script>
<script type="text/javascript">
	$(".select2").select2();
</script>
@endpush
@endsection