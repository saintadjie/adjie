@extends('layouts.masterfe')
@push('script-header')
        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('assetsfe/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
      
@endpush
@section('content')
<div class="content-wrapper">
	<div class="col-12">
	    <div class="card">
	        <div class="card-body">
	            
	            <div class="row">
	                <div class="col-md-12 mx-auto">
	                  	<ul class="nav nav-pills nav-pills-custom" id="pills-tab-custom" role="tablist">
	                        <li class="nav-item">
	                          	<a class="nav-link active" id="pills-home-tab-custom" data-toggle="pill" href="#global" role="tab" aria-controls="pills-home" aria-selected="true">
	                            	Global
	                          	</a>
	                        </li>
	                        <li class="nav-item">
	                          	<a class="nav-link" id="pills-profile-tab-custom" data-toggle="pill" href="#lokal" role="tab" aria-controls="pills-profile" aria-selected="false">
	                            	Indonesia
	                          	</a>
	                        </li>
	                  	</ul>
	                  	<div class="tab-content tab-content-custom-pill" id="pills-tabContent-custom">
	                    	<div class="tab-pane fade show active" id="global" role="tabpanel" aria-labelledby="pills-home-tab-custom">
	                      		<h4 class="card-title">Info Corona Global</h4>
					            <p class="card-description">Informasi ini didapatkan dari API <code class="highlighter-rouge">https://corona.lmao.ninja/</code>.</p>
	                          	<div class="row">
	            					<div class="col-lg-12 grid-margin stretch-card">
	              						<div class="card">
							        		<div class="card-body">
							        			<div class="row">

									                <div class="col-lg-3">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #f82649;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Total Kasus</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsglobal->cases}}</h2>
									                                        <h5 class="text-white mb-0">Orang</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-led-on mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>

									                <div class="col-lg-3">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #fc7303;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Aktif</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsglobal->active}}</h2>
									                                        <h5 class="text-white mb-0">Kasus</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-bell mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>

									                <div class="col-lg-3">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #09ad95;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Total Sembuh</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsglobal->recovered}}</h2>
									                                        <h5 class="text-white mb-0">Orang</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-heart mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>

									                <div class="col-lg-3">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #d43f8d;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Total Meninggal</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsglobal->deaths}}</h2>
									                                        <h5 class="text-white mb-0">Orang</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-heart-broken mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>
									            </div>
							        		</div>
							        	</div>
	            					</div>
	            
	        						<div class="col-lg-12 grid-margin stretch-card">
	              						<div class="card">
	                                        <div class="card-body">
	                                            <h4 class="header-title">Daftar Negara Terjangkit</h4>
	                                            <p class="card-title-desc">
	                                                    Terdapat : <code> {{$hitungnegara}} </code> Negara Terjangkit Covid19.
	                                            </p>
	                                            <div class="table-responsive">
	                                                <table id="tb_negara" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
	                                                    <thead>
	                                                        <tr>
	                                                            <th style="vertical-align: middle; text-align: center;">No</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Negara</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Kasus</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Kasus hari ini</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Kematian</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Kematian hari ini</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Pulih</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Aktif</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Kritis</th>
	                                                        </tr>
	                                                    </thead>
	                                                    <tbody>
	                                                        @foreach($rsnegara as $resultnegara)
	                                                        <tr id="{{$resultnegara->id}}">
	                                                            <td style="vertical-align: middle; text-align: center;"></td>
	                                                            <td style="vertical-align: middle; text-align: left;">{{$resultnegara->negara}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->kasus}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->kasushariini}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->kematian}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->kematianhariini}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->pulih}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->aktif}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultnegara->kritis}}</td>
	                                                        </tr>
	                                                        @endforeach
	                                                    </tbody>
	                                                </table>
	                                            </div>
	                                        </div>
	                                    </div>
						            </div>
						        </div>
	                    	</div>
	                        <div class="tab-pane fade" id="lokal" role="tabpanel" aria-labelledby="pills-profile-tab-custom">
	                        	<h4 class="card-title">Info Corona Indonesia</h4>
					            <p class="card-description">Informasi ini didapatkan dari API <code class="highlighter-rouge">https://kawalcorona.com/</code>.</p>
	                          	<div class="row">
	            					<div class="col-lg-12 grid-margin stretch-card">
	              						<div class="card">
							        		<div class="card-body">
							        			<div class="row">

									                <div class="col-lg-4">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #f82649;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Total Positif</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsindo[0]->positif}}</h2>
									                                        <h5 class="text-white mb-0">Orang</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-led-on mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>

									                <div class="col-lg-4">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #09ad95;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Total Sembuh</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsindo[0]->sembuh}}</h2>
									                                        <h5 class="text-white mb-0">Orang</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-heart mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>

									                <div class="col-lg-4">
									                    <div class="card">
									                        <ul class="list-group list-group-flush">
									                            <li class="list-group-item" style="background-color: #d43f8d;">
									                                <div class="media my-2">
									                                    
									                                    <div class="media-body">
									                                        <h5 class="text-white mb-0">Total Meninggal</h5>
									                                        <h2 class="text-white mb-0 count">{{$rsindo[0]->meninggal}}</h2>
									                                        <h5 class="text-white mb-0">Orang</h5>
									                                    </div>
									                                    <div class="icons-lg ml-2 align-self-center">
									                                        <i class="mdi mdi-heart-broken mdi-36px" style="color:#fff;"></i>
									                                    </div>
									                                </div>
									                            </li>
									                        </ul>
									                    </div>
									                </div>
									            </div>
							        		</div>
							        	</div>
	            					</div>
	            
	        						<div class="col-lg-12 grid-margin stretch-card">
	              						<div class="card">
	                                        <div class="card-body">
	                                            <h4 class="header-title">Daftar Provinsi Terjangkit</h4>
	                                            <p class="card-title-desc">
	                                                    Terdapat : <code> {{$hitungprovinsi}} </code> Provinsi Terjangkit Covid19.
	                                            </p>
	                                            <div class="table-responsive">
	                                                <table id="tb_provinsi" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
	                                                    <thead>
	                                                        <tr>
	                                                            <th style="vertical-align: middle; text-align: center;">No</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Provinsi</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Positif</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Sembuh</th>
	                                                            <th style="vertical-align: middle; text-align: center;">Meninggal</th>
	                                                        </tr>
	                                                    </thead>
	                                                    <tbody>
	                                                        @foreach($rsprovinsi as $resultprovinsi)
	                                                        <tr id="{{$resultprovinsi->FID}}">
	                                                            <td style="vertical-align: middle; text-align: center;"></td>
	                                                            <td style="vertical-align: middle; text-align: left;">{{$resultprovinsi->provinsi}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultprovinsi->positif}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultprovinsi->sembuh}}</td>
	                                                            <td style="vertical-align: middle; text-align: center;">{{$resultprovinsi->meninggal}}</td>
	                                                        </tr>
	                                                        @endforeach
	                                                    </tbody>
	                                                </table>
	                                            </div>
	                                        </div>
	                                    </div>
						            </div>
						         </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@push('script-footer')
        <script src="{{url('assetsfe/vendors/datatables.net/jquery.dataTables.js')}}"></script>
		<script src="{{url('assetsfe/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
		<script src="{{url('assetsfe/js/data-table.js')}}"></script>

        <script type="text/javascript">

            $('.count').each(function () {
            	var num =   $(this).text().replace(',', '');
                $(this).prop('Counter',0).animate({
                    Counter: num
                }, {
                    duration: 3000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now).toLocaleString('id'));
                    }
                });
            });
        </script>

        <script type="text/javascript">

            var t = $('#tb_negara').DataTable({
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
                "order": [[ 2, "desc" ]],
            });

            t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            var u = $('#tb_provinsi').DataTable({
                "language": {
                    "emptyTable":     "Tidak ada data yang tersedia",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(tersaring dari _MAX_ total data)",
                    "lengthMenu":     "Tampilkan _MENU_ data",
                    "search":         "Pencarian:",
                    "zeroRecords":    "Pencarian tidak ditemukan",
                    "paginate": {
                        "first":      "Awal",
                        "last":       "Akhir",
                        "next":       "▶",
                        "previous":   "◀"
                    },
                },
                "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
                "order": [[ 2, "desc" ]],
            });

            u.on( 'order.dt search.dt', function () {
                u.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
        </script>
@endpush
@endsection