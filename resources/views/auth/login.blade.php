

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Adjie - Login</title>
    <!-- base:css -->
    <link rel="stylesheet" href="{{url('assetsfe/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{url('assetsfe/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{url('assetsfe/css/horizontal-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{url('assetsfe/images/logo.png')}}" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="main-panel">
                <div class="content-wrapper d-flex align-items-center auth px-0">
                    <div class="row w-100 mx-0">
                        <div class="col-lg-4 mx-auto">
                            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                                <div class="brand-logo">
                                    <img src="{{url('assetsfe/images/logolight.png')}}" alt="logo">
                                </div>
                                <h4>Halo! Selamat datang</h4>
                                <h6 class="font-weight-light">Silahkan login.</h6>
                                <form class="pt-3" method="POST" action="{{ route('login') }}">
                                    @csrf                                    @if ($errors->has('email') || $errors->has('password'))
                                        <div class="alert alert-danger">
                                            <center>
                                                <strong>{{ $errors->first('email') ?: $errors->first('password')}}</strong>
                                            </center>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="E-Mail" required autocomplete="email" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                                    </div>
                                    <div class="mt-3">
                                        <button name="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">{{ __('Login') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <!-- content-wrapper ends -->
        </div>
    <!-- page-body-wrapper ends -->
    </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="{{url('assetsfe/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{url('assetsfe/js/off-canvas.js')}}"></script>
  <script src="{{url('assetsfe/js/hoverable-collapse.js')}}"></script>
  <script src="{{url('assetsfe/js/template.js')}}"></script>
        <script src="{{url('assetsfe/js/settings.js')}}"></script>
        <script src="{{url('assetsfe/js/todolist.js')}}"></script>
  <!-- endinject -->
</body>

</html>
