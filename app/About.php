<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'ms_about';

    protected $fillable = [
        'id',
    	'name',
    	'website',
    	'email',
    	'instagram',
    	'facebook',
    	'deskripsi',
    	'photo',
    	'penulis'
    ];
}
