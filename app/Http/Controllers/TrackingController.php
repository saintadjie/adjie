<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrackingController extends Controller
{
    public function index()
	{

		return view('tracking');
	}

    public function tracking(Request $request)
    {
    	// Kaskus HT
    	$curl = curl_init();

    	$nomorresi = $request->nomorresi;
    	$api = '397c7c733776209d9fbd09fbf50d3bea4f3cde4cd063ae3446d84b9a3e47573f';
    	$kurir = $request->kurir;
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.binderbyte.com/cekresi?awb=$nomorresi&api_key=$api&courier=$kurir",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        if (curl_errno($curl)){
        	$err 		= curl_error($curl);
        }
        if (isset($err)){
        	abort(503);
        }

        $hasil = curl_exec($curl);
        
        curl_close($curl);
        
        $rs = json_decode($hasil);

        if (($rs) != true) return abort(404);
        if (($rs->result) != true) return abort(404);
        $rsdata = $rs->data;
        $rstracking = $rs->data->tracking;
        foreach ($rstracking as $key => $value) {
            $value->date = $value->date;
        }
        
        return view('trackingdetail', ['rs' => $rs, 'rsdata' => $rsdata, 'rstracking' => $rstracking]);
    }
}
