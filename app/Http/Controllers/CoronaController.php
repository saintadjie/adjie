<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CoronaController extends Controller
{
    public function corona()
    {
    	// Corona Global
    	$curlglobal = curl_init();

        curl_setopt_array($curlglobal, array(
            CURLOPT_URL => "https://corona.lmao.ninja/v2/all",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

        if (curl_errno($curlglobal)){
            $errglobal      = curl_error($curlglobal);
        }

        if (isset($errglobal)){
            abort(503);
        }

        $hasilglobal    = curl_exec($curlglobal);

        curl_close($curlglobal);

        $rsglobal       = json_decode($hasilglobal);
        // dd($rsglobal);

        // Corona Indonesia
        $curlindo = curl_init();

        curl_setopt_array($curlindo, array(
            CURLOPT_URL => "https://api.kawalcorona.com/indonesia/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

        if (curl_errno($curlindo)){
            $errindo        = curl_error($curlindo);
        }

        if (isset($errindo)){
            abort(503);
        }

        $hasilindo  = curl_exec($curlindo);

        curl_close($curlindo);

        $rsindo     = json_decode($hasilindo);
        // dd($rsindo);
        

        // Corona Negara-Negara Dunia
    	$curlnegara = curl_init();

        curl_setopt_array($curlnegara, array(
            CURLOPT_URL => "https://corona.lmao.ninja/v2/countries?yesterday=false",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

        if (curl_errno($curlnegara)){
            $errnegara      = curl_error($curlnegara);
        }
        

        if (isset($errnegara)){
            abort(503);
        }

        $hasilnegara = curl_exec($curlnegara);

        curl_close($curlnegara);

        $rsnegara = json_decode($hasilnegara);
        // dd($rsnegara);
        $hitungnegara = count($rsnegara);
        
        foreach ($rsnegara as $key => $value) {
            $data = array_search($value->country, array_column($rsnegara, 'country'));
            
            $value->id = $rsnegara[$data]->countryInfo->_id;
            $value->negarax = $value->country;
            $value->negara = $rsnegara[$data]->country;
            $value->kasus = $rsnegara[$data]->cases;
            $value->kasushariini = $rsnegara[$data]->todayCases;
            $value->kematian = $rsnegara[$data]->deaths;
            $value->kematianhariini = $rsnegara[$data]->todayDeaths;
            $value->pulih = $rsnegara[$data]->recovered;
            $value->aktif = $rsnegara[$data]->active;
            $value->kritis = $rsnegara[$data]->critical;
        }
        // dd($rs);
        
   
        // Corona Provinsi Indonesia

        $curlprovinsi = curl_init();

        curl_setopt_array($curlprovinsi, array(
            CURLOPT_URL => "https://api.kawalcorona.com/indonesia/provinsi/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

        if (curl_errno($curlprovinsi)){
            $errprovinsi        = curl_error($curlprovinsi);
        }

        if (isset($errprovinsi)){
            abort(503);
        }

        $hasilprovinsi = curl_exec($curlprovinsi);

        curl_close($curlprovinsi);

        $rsprovinsi = json_decode($hasilprovinsi);
        // dd($rsprovinsi);
        $hitungprovinsi = count($rsprovinsi);
        
        foreach ($rsprovinsi as $key => $value) {
            $data = array_search($value->attributes, array_column($rsprovinsi, 'attributes'));
            $value->FID = $rsprovinsi[$data]->attributes->FID;
            $value->provinsix = $value->attributes->Provinsi;
            $value->provinsi = $rsprovinsi[$data]->attributes->Provinsi;
            $value->positif = $rsprovinsi[$data]->attributes->Kasus_Posi;
            $value->sembuh = $rsprovinsi[$data]->attributes->Kasus_Semb;
            $value->meninggal = $rsprovinsi[$data]->attributes->Kasus_Meni;
        }
        

        return view('corona', ['rsglobal' => $rsglobal, 'rsindo' => $rsindo, 'rsnegara' => $rsnegara, 'hitungnegara' => $hitungnegara, 'rsprovinsi' => $rsprovinsi, 'hitungprovinsi' => $hitungprovinsi]);
        
    }

   
}
