<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class KaskusController extends Controller
{
	private function __getDataKaskus($limit=50) 
    {
    	// Kaskus HT
    	$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://kaskus-ht-api.now.sh/api?limit=$limit",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        if (curl_errno($curl)){
        	$err 		= curl_error($curl);
        }
        if (isset($err)){
        	return false;
        }
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);
        $result = [];
        foreach ($data->result as $key => $value) {
        	foreach($value as $k => $v) {
        		array_push($result, $v);
        	}
        }
        return $result;
    }

    private function __findData($data=[], $slug='') 
    {
    	if (!is_array($data) && count($data) === 0) return false;
    	$filter = array_filter(
    		$data,
    		function ($v) use ($slug) { return $v->slug_title === $slug; }
    	);
    	if (count($filter) === 0) return false;
    	$keys = array_keys($filter);
    	$result = $filter[$keys[0]];
    	return $result;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 9, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function index()
    {
    	$data = $this->__getDataKaskus(50);
    	$paginate = $this->paginate($data);
    	$paginate->withPath('thread');
    	if (is_bool($data)) return abort(404);
    	// dd($paginate);
        return view('thread', ['result' => $paginate]);
    }

    public function show($slug_title)
    {
    	$data = $this->__getDataKaskus(50);
    	if (is_bool($data) && !$data) return abort(404);
    	// dd($this->__findData($data, $slug_title));
    	$filteredData = $this->__findData($data, $slug_title);
    	if (is_bool($filteredData)) return abort(404);
        return view('threaddetail', ['result' => $filteredData]);
    }
}
