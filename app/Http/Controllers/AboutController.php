<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\About;
use Image;
use File;

class AboutController extends Controller
{

    public function about() {

    	$rs     = About::select('id', 'name', 'website', 'email', 'instagram', 'facebook', 'deskripsi', 'photo', 'penulis')
                	->first();

        return view('about', ['rs' => $rs]);

    }

    public function aboutadmin() {

        $rs     = About::select('id', 'name', 'website', 'email', 'instagram', 'facebook', 'deskripsi', 'photo', 'penulis')
                    ->first();
        return view('administrator/about', ['rs' => $rs]);

    }

    public function saveabout(Request $request) {

        $path           = null;
        $check          = About::select('id')
                            ->get();
        $hitungcheck    = count($check);
        

        if (($hitungcheck == '0')) {

            if ($request->hasFile('photo')) {
                $dir = public_path() . "/images/about/$request->name";
                if (!is_dir($dir)) {
                    File::makeDirectory($dir, $mode = 0777, true, true);         
                }
                Image::make($request->file('photo'))->resize(300, 300)->save($dir . '/' . $request->name . '.jpg', 100);
                $path = 'images/about/' . $request->name . '/' . $request->name . '.jpg';
            }

            $about              = new About();
            $about->name        = $request->name;
            $about->website     = $request->website;
            $about->email       = $request->email;
            $about->instagram   = $request->instagram;
            $about->facebook    = $request->facebook;
            $about->deskripsi   = $request->deskripsi;
            $about->photo       = $path != null ? $path : null;
            $about->penulis     = $request->penulis;
            $about->save();

            return response()->json(['status' => 'OK']);

        } else{

            $decodecheck    = json_decode($check);
            $idabout        = $decodecheck[0]->id;

            if ($request->hasFile('photo')) {
                $dir = public_path() . "/images/about/$request->name";
                if (!is_dir($dir)) {
                    File::makeDirectory($dir, $mode = 0777, true, true);         
                }
                Image::make($request->file('photo'))->resize(150, 150)->save($dir . '/' . $request->name . '.jpg', 100);
                $path = 'images/about/' . $request->name . '/' . $request->name . '.jpg';
            }

            $about              = About::find($idabout);
            $path               = $about->photo != null && $path == null ? $about->photo : $path; 
            $about->name        = $request->name;
            $about->website     = $request->website;
            $about->email       = $request->email;
            $about->instagram   = $request->instagram;
            $about->facebook    = $request->facebook;
            $about->deskripsi   = $request->deskripsi;
            $about->photo       = $path != null ? $path : null;
            $about->penulis     = $request->penulis;
            $about->save();

            return response()->json(['status' => 'OK']);
            
        }
    }
}
