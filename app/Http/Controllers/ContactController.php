<?php

namespace App\Http\Controllers;
use Mail;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){

        return view('contact');

    }
    

    public function sendemail(Request $request)
	{
		
	    try{
	        Mail::send('email', ['email' => $request->email ,'name' => $request->name, 'pesan' => $request->pesan], function ($message) use ($request)
	        {
	            $message->subject($request->judul);
	            $message->from('saintadjie@gmail.com','adjie.id');
	            $message->to(['saintadjie@gmail.com'])->replyTo($request->email, $request->name);
	        });
	        return response()->json(['status' => 'OK']);
	    }
	    catch (Exception $e){
	        return response (['status' => false,'errors' => $e->getMessage()]);
	    }
	}
}
