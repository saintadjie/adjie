<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $data = [
            'id' => '1',
            'name' => 'Adjie Krisnandy',
            'email' => 'saintadjie@gmail.com',
            'password' => '$2y$10$sKAvhqC9l0vmKFB6aYEySOmIfsg9Ns4qHDh8TWV/HTcWWqMJ92zwe',
        ];
        $user->fill($data);
        $user->save();
    }
}
