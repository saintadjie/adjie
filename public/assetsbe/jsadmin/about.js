$(function () {

    $('#btn_simpan').click(function(){
        if ($('#name').val() == '') {
            Swal.fire( "Kesalahan", "Nama tidak boleh kosong", "error" )
            return false;
        } else if ($('#website').val() == '') {
            Swal.fire( "Kesalahan", "Website tidak boleh kosong", "error" )
            return false;
        } else if ($('#email').val() == '') {
            Swal.fire( "Kesalahan", "Email tidak boleh kosong", "error" )
            return false;
        }


        var uploadfile = new FormData($("#form_about")[0])

        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
        }).done(function(data){

            if(data.status == 'OK'){
                Swal.fire({
                    icon: 'success',
                    title: "Data telah disimpan",
                    timer: 3000,
                    showConfirmButton: true,
                    html: 'Otomatis tertutup dalam <b></b> milidetik.',
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                          const content = Swal.getContent()
                          if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                              b.textContent = Swal.getTimerLeft()
                            }
                          }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(function () {
                    location.href = url_about
                })

            }

        })
    })

})
