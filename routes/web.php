<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/', function () {
    return view('index');
});

Route::GET('/about', 'AboutController@about')->name('about');
Route::GET('/contact', 'ContactController@index')->name('contact');
Route::GET('/corona', 'CoronaController@corona')->name('corona');
Route::GET('/disclaimer', 'DisclaimerController@index')->name('disclaimer');
Route::GET('/privacypolicy', 'PrivacyPolicyController@index')->name('privacypolicy');
Route::GET('/thread', 'KaskusController@index')->name('thread');
Route::GET('/threaddetail/{slug_title}', 'KaskusController@show')->name('threaddetail');
Route::GET('/tracking', 'TrackingController@index')->name('tracking');
Route::GET('/trackingdetail', 'TrackingController@tracking')->name('trackingdetail');

Auth::routes([
  	'register' => false, // Registration Routes...
  	'reset' => false, // Password Reset Routes...
  	'verify' => false, // Email Verification Routes...
]);

Route::GET('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function () {

	Route::GET('home', 'HomeController@index')->name('home');
    Route::GET('about', 'AboutController@aboutadmin');

});


Route::POST('/sendemail', 'ContactController@sendemail');